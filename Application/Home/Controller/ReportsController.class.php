<?php

namespace Home\Controller;

use Think\Controller;

class ReportsController extends Controller
{
    public function index()
    {
        $this->display();
    }

    public function get_precision($model_id, $weights_id, $class_id, $thresh = 0.2)
    {
        $Model = new \Think\Model();
        $sql = "SELECT count(*) FROM gt_frame
JOIN results ON gt_frame.frame_id = results.frame_id
WHERE results.class_id = " . $class_id . " AND results.movie_id = 1
AND gt_frame.class_id = " . $class_id . " AND gt_frame.gt=1
AND model_id = " . $model_id . " AND weights_id = " . $weights_id . "
AND results.confidence >" . $thresh;
        $ret = $Model->query($sql);
        return $ret[0]['count(*)'];
    }

    public function get_weights()
    {
        $Model = new \Think\Model();
        $sql =  'select weights.name, statistic.class_id, statistic.movie_id, classes.name as class_name, movie.name as movie_name,data from statistic 
join weights on statistic.weights_id = weights.id
join classes on classes.id = statistic.class_id
join movie on movie.id = statistic.movie_id
where tp>0 order by classes.id, weights.id';
        $ret = $Model->query($sql);
        for ($i = 0; $i < count($ret); $i++) {
            $ret[$i]['data'] = json_decode($ret[$i]['data']);
        }
        echo json_encode($ret);
//        $weights = M("weights")->where('done=1')->select();
//        for ($i = 0; $i < count($weights); $i++) {
//            $statistic = M("statistic")->where('weights_id=' . $weights[$i]['id'] . ' and model_id=' . $weights[$i]['model_id'])->find();
//            $data = json_decode($statistic['data']);
//            $weights[$i]['data'] = $data;
//        }
//        echo json_encode($weights);
    }

    #True Positive 本来是苹果的分类成苹果
    /* select count(*) from results
    join frames on results.frame_id = frames.id
    join gt_frame on gt_frame.frame_id = frames.id
    where weights_id = 1 and results.movie_id = 1 and results.class_id = 0 and gt_frame.gt = 1  */

    public function get_tp($movie_id, $weights_id, $class_id, $thresh)
    {
        $Model = new \Think\Model();
        $sql = "SELECT count(*) FROM results
    JOIN frames ON results.frame_id = frames.id
    JOIN gt_frame ON gt_frame.frame_id = frames.id
    WHERE weights_id = " . $weights_id . " AND results.movie_id = " . $movie_id . " AND results.class_id = " . $class_id . " AND results.confidence  > " . $thresh . " AND gt_frame.gt = 1";
        echo $sql . "</br>";
        $ret = $Model->query($sql);
        return $ret[0]['count(*)'];
    }

    #False Positive 本来不是苹果被分类成苹果
    /* select count(*) from results
    join frames on results.frame_id = frames.id
    join gt_frame on gt_frame.frame_id = frames.id
    where weights_id = 1 and results.movie_id = 1 and results.class_id = 0 and gt_frame.gt = 0  */

    public function get_fp($movie_id, $weights_id, $class_id, $thresh)
    {
        $Model = new \Think\Model();
        $sql = "SELECT count(*) FROM results
    JOIN frames ON results.frame_id = frames.id
    JOIN gt_frame ON gt_frame.frame_id = frames.id
    WHERE weights_id = " . $weights_id . " AND results.movie_id = " . $movie_id . " AND results.class_id = " . $class_id . " AND results.confidence  > " . $thresh . " AND gt_frame.gt = 0";
        echo $sql . "</br>";
        $ret = $Model->query($sql);
        return $ret[0]['count(*)'];
    }

    public function get_total_gt($class_id)
    {
        $Model = new \Think\Model();
        $sql = "SELECT count(*) FROM gt_frame WHERE gt = 1 AND class_id = " . $class_id;
        echo $sql . "</br>";
        $ret = $Model->query($sql);
        return $ret[0]['count(*)'];
    }

    public function get_total_results($movie_id, $weights_id, $class_id, $thresh)
    {
        $Model = new \Think\Model();
        $sql = "select count(*) from results where weights_id = " . $weights_id . " and results.movie_id = " . $movie_id . " and results.class_id = " . $class_id . " and results.confidence  > " . $thresh;
        echo $sql . "</br>";
        $ret = $Model->query($sql);
        return $ret[0]['count(*)'];
    }

    public function get_report_data($id, $movie_id, $model_id, $weights_id, $class_id)
    {
        $statistic = M("statistic");
        $res = array();
        for ($i = 2; $i < 10; $i++) {
            $value['tp'] = $this->get_tp($movie_id, $weights_id, $class_id, $i*0.1);
            if ($value['tp'] == 0)
                continue;
            $value['fp'] = $this->get_fp($movie_id, $weights_id, $class_id, $i*0.1);
            $value['tn'] = $this->get_total_results($movie_id, $weights_id, $class_id, $i*0.1) - $value['tp'];
            $value['fn'] = $this->get_total_gt($class_id) - $value['tp'];
            $value['precision'] = $value['tp']/($value['tp'] + $value['tn']);
            $value['recall'] = $value['tp']/($value['tp'] + $value['fn']);
            array_push($res, $value);
            echo $value['recall'];
            echo "</br></br></br>";
        }

        $data['model_id'] = $model_id;
        $data['weights_id'] = $weights_id;
        $data['class_id'] = $class_id;
        $data['movie_id'] = $movie_id;
        $data['tp'] = $this->get_tp($movie_id, $weights_id, $class_id, 0.2);
        $data['data'] = json_encode($res);
        $s = $statistic->field('weights_id,movie_id,model_id,class_id,tp,data')->add($data);
        $result['id'] = $id;
        $result['data'] = $res;
//    $result['statistic'] = $data;
//    $result['ret'] = $s;
//        echo json_encode($result);
    }

    public function cacl($class_id = 0, $movie_id = 1)
    {
        M("statistic")->where('1')->delete();
        $weights = M("weights")->where('done=1')->select();
        for ($i = 0; $i < count($weights); $i++) {
            $this->get_report_data($i, $movie_id, $weights[$i]['model_id'], $weights[$i]['id'], $class_id);
        }
    }

    public function cacl1()
    {
        M("statistic")->where('1')->delete();
        $Model = new \Think\Model();
        $sql = 'select DISTINCT weights_id, model_id, movie_id from results';
        $ret = $Model->query($sql);
        for ($i = 0; $i < count($ret); $i++) {
            $movie_id = $ret[$i]['movie_id'];
            $weights_id = $ret[$i]['weights_id'];
            $model_id = $ret[$i]['model_id'];
            $classes = M("classes")->where("movie_id=" . $movie_id)->find();
            $class_id = $classes['id'];
            $this->get_report_data($i, $movie_id, $model_id, $weights_id, $class_id);

//            $ret = M("statistic")->where()->find();
//            if (count($ret) == 0)
//                $this->get_report_data($i, $movie_id, $model_id, $weights_id, $class_id);
        }
    }
}
