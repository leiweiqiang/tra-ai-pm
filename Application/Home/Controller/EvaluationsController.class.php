<?php

namespace Home\Controller;

use Think\Controller;

class EvaluationsController extends Controller
{
    public function index()
    {
        $this->display();
    }

    public function get_data()
    {
        $weights = M("weights")->select();
        $movies = M("movie")->select();
        $ret['weights'] = $weights;
        $ret['movies'] = $movies;
        echo json_encode($ret);
    }

    public function get_testing_data($weights = 0, $positives = 0, $negatives = 0)
    {
        $Model = new \Think\Model();
//        $sql = 'SELECT * FROM iou WHERE $weights = $weights and (movie_id = $positives or movie_id = $negatives) and confidence > 0.2';
        $sql = 'SELECT annotation.*, movie.table_name FROM testing_annotation
JOIN annotation ON annotation.id = testing_annotation.annotation_id
JOIN movie ON annotation.movie_id = movie.id
WHERE 
(annotation.movie_id = $positives OR annotation.movie_id = $negatives)
ORDER BY annotation.frame_index';

//        $sql = str_replace('$weights', $weights, $sql);
        $sql = str_replace('$positives', $positives, $sql);
        $sql = str_replace('$negatives', $negatives, $sql);
        $ret = $Model->query($sql);

        $data['testing_data'] = $ret;
        $data['results'] = $this->load_results_data($weights, $positives, $negatives);

        $positives_class = M("classes")->where('movie_id = ' . $positives)->find();
        $data['positive_class'] = $positives_class['id'];
        echo json_encode($data);
    }

    public function get_iou($result)
    {

        $x1 = $result['x1'];
        $y1 = $result['y1'];
        $x2 = $result['x2'];
        $y2 = $result['y2'];

        $sql = 'SELECT annotation.* FROM testing_annotation
JOIN annotation ON annotation.id = testing_annotation.annotation_id
WHERE annotation.frame_id =' . $result['frame_id'];

        $Model = new \Think\Model();
        $ret = $Model->query($sql);

        $_x1 = $ret[0]['x1'];
        $_y1 = $ret[0]['y1'];
        $_x2 = $ret[0]['x2'];
        $_y2 = $ret[0]['y2'];

        $areai = ($x2 - $x1 + 1) * ($y2 - $y1 + 1);
        $areaj = ($_x2 - $_x1 + 1) * ($_y2 - $_y1 + 1);

        $xx1 = max($x1, $_x1);
        $yy1 = max($y1, $_y1);
        $xx2 = min($x2, $_x2);
        $yy2 = min($y2, $_y2);

        $h = max(0, $yy2 - $yy1 + 1);
        $w = max(0, $xx2 - $xx1 + 1);

        $intersection = $w * $h;

        $iou = $intersection / ($areai + $areaj - $intersection);

        return $iou;
    }

    public function load_results_data($weights_id = 0, $positives = 0, $negatives = 0)
    {
        $positives_class = M("classes")->where('movie_id = ' . $positives)->find();
        $negatives_class = M("classes")->where('movie_id = ' . $negatives)->find();

        $where['weights_id'] = $weights_id;
        $where['_string'] = 'movie_id = ' . $positives . ' or movie_id = ' . $negatives;
        $where['_string'] = 'class_id = ' . $positives_class['id'] . ' or class_id = ' . $negatives_class['id'] ;
        $where['confidence'] = array('gt', 0.2);
        $ret = M("iou")->where($where)->select();
        for ($i = 0; $i < count($ret); $i++) {
            $ret[$i]['iou'] = $this->get_iou($ret[$i]);
        }
        return $ret;
    }

//    public function get_iou_results($weights = 0, $positives = 0, $negatives = 0){
//        $positives_class = M("classes")->where('movie_id = ' . $positives)->find();
//        $negatives_class = M("classes")->where('movie_id = ' . $negatives)->find();
//        $sql = 'SELECT * FROM iou
//WHERE weights_id = $weights
//and (movie_id = $positives or movie_id = $negatives)
//and (class_id = $pclass or class_id = $nclass)
//and confidence > 0.2';
//        $sql = str_replace('$weights', $weights, $sql);
//        $sql = str_replace('$positives', $positives, $sql);
//        $sql = str_replace('$negatives', $negatives, $sql);
//        $sql = str_replace('$pclass', $negatives['id'], $sql);
//        $sql = str_replace('$nclass', $negatives['id'], $sql);
//
//        $Model = new \Think\Model();
//        $ret = $Model->query($sql);
//        return $ret;
//    }


}
