<?php

namespace Home\Controller;

use Think\Controller;

class IndexController extends Controller
{
    public function index()
    {
        $this->display();
    }

    public function dashboard()
    {
        $this->display();
    }

    public function upload_json_results()
    {
        $this->display();
    }
}
