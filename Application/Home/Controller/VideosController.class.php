<?php

namespace Home\Controller;

use Think\Controller;

class VideosController extends Controller
{
    public function index()
    {
        $this->display();
    }

    public function get_all()
    {
        $imdb = M("imdb")->select();
        for ($i = 0; $i < count($imdb); $i++) {
            $imdb[$i]['imdb_info'] = json_decode($imdb[$i]['imdb_info']);
            $movie = M("movie")->where("imdb_id='" . $imdb[$i]['imdb_id'] . "'")->find();
//            $imdb[$i]['created'] = count($movie) > 0;
        }
        echo json_encode($imdb);
    }

    public function create_frame(){
        C('SHOW_PAGE_TRACE', false);
        if (!IS_POST) {
            return false;
        }
        if (IS_POST) {
            $imdb = json_decode(file_get_contents("php://input"), true);
            $movie = M("movie")->where("imdb_id='" . $imdb['imdb_id'] . "'")->find();
            $data['imdb_id'] = $imdb['imdb_id'];
            if (count($movie) == 0){
                $ret = M("movie")->field('imdb_id')->add($data);
            }
            else{
                $data['name'] = $imdb['imdb_info']['Title'];
                $name = strtolower($imdb['imdb_info']['Title']);
                $name = preg_replace("/[^A-Za-z0-9\s+]/", "", $name);
                $name = preg_replace('/\s+/', '_', $name);
                $data['table_name'] =  $movie['id'] . '_' . $name;
                $name = $imdb['imdb_info']['Title'];
                $name = preg_replace("/[^A-Za-z0-9\s+]/", "", $name);
                $name = preg_replace('/\s+/', '-', $name);
                $data['prefix'] =  $movie['id'] . '-' . $name . '-' . $imdb['imdb_info']['Year'] . '-';
                $data['filename'] =  $imdb['filename'];
                $data['image'] =  $imdb['imdb_info']['Poster'];
                $ret = M("movie")->where("imdb_id='" . $imdb['imdb_id'] . "'")->field('imdb_id,name,table_name,prefix,image,filename')->save($data);
            }
            echo json_encode($imdb);
        }
    }
}
