<?php

namespace Home\Controller;

use Think\Controller;

class AnnotationController extends Controller
{

    public function index($class_id = 0)
    {
        $class = M("classes")->where('id=' . $class_id)->find();
        $this->assign('name', $class['name']);
        $this->assign('class_id', $class_id);
        $this->assign('movie_id', $class['movie_id']);
        $this->display();
    }

    public function pending($class_id = 0, $round = 0)
    {
        $class = M("classes")->where('id=' . $class_id)->find();
        $this->assign('name', $class['name']);
        $this->assign('class_id', $class_id);
        $this->assign('round', $round);
        $this->display();
    }

    public function edit($annotation_id = 0)
    {
        $annotation = M("annotation")->where('id=' . $annotation_id)->find();
        $class = M("classes")->where('id=' . $annotation['class_id'])->find();
        $movie = M("movie")->where('id=' . $annotation['movie_id'])->find();

        $this->assign('annotation_id', $annotation_id);
        $this->assign('class_name', $class['name']);
        $this->assign('class_id', $class['id']);
        $this->assign('movie_name', $movie['name']);
        $this->assign('image_path', C('IMAGE_PATH') . $movie['table_name'] . '/all_frame/');
        $this->assign('filename', $annotation['filename']);

        $this->display();
    }

    public function create($class_id = 0, $minute = 0)
    {
        $class = M("classes")->where('id=' . $class_id)->find();
        $movie = M("movie")->where('id=' . $class['movie_id'])->find();

        $this->assign('minute', $minute);
        $this->assign('class_id', $class_id);
        $this->assign('w', $movie['w']);
        $this->assign('h', $movie['h']);
        $this->assign('class_name', $class['name']);
        $this->assign('movie_name', $movie['name']);
        $this->assign('movie_id', $class['movie_id']);

        $this->display();
    }


    public function create_annotation_from_keyframe($class_id = 0)
    {
        $class = M("classes")->where('id=' . $class_id)->find();
        $movie = M("movie")->where('id=' . $class['movie_id'])->find();

        $this->assign('class_id', $class_id);
        $this->assign('w', $movie['w']);
        $this->assign('h', $movie['h']);
        $this->assign('class_name', $class['name']);
        $this->assign('movie_name', $movie['name']);
        $this->assign('movie_id', $class['movie_id']);

        $this->display();
    }


    public function add($class_id = 0, $movie_id = 0, $filename = '')
    {
        $class = M("classes")->where('id=' . $class_id)->find();
        $movie = M("movie")->where('id=' . $class['movie_id'])->find();

        $this->assign('class_id', $class_id);
        $this->assign('movie_id', $movie_id);
        $this->assign('class_name', $class['name']);
        $this->assign('movie_name', $movie['name']);
        $this->assign('image_path', C('IMAGE_PATH') . $movie['table_name'] . '/all_frame/');
        $this->assign('filename', $filename);

        $this->display();
    }

    public function getc($class_id)
    {
        $Model = new \Think\Model();
//        $sql = 'SELECT annotation.*, movie.table_name  FROM annotation JOIN movie ON movie.id = annotation.movie_id WHERE annotation.id NOT IN (SELECT annotation_id AS id FROM training_annotation) AND annotation.movie_id = $movie_id AND annotation.class_id = $class_id AND round < 0 ORDER BY rand() LIMIT $total';
        $sql = 'SELECT annotation.*, movie.table_name  FROM annotation JOIN movie ON movie.id = annotation.movie_id WHERE annotation.class_id = $class_id AND round < 0 ORDER BY frame_index';
        $sql = str_replace('$class_id', $class_id, $sql);
        $ret = $Model->query($sql);
        echo json_encode($ret);
//
//
//        $file_list = M("annotation")->distinct(true)->field('id,frame_index,filename,movie_id,w,h,x1,y1,x2,y2,round')->where('class_id=' . $class_id . ' and (round<0)')->order('frame_index')->select();
//        for ($i = 0; $i < count($file_list); $i++) {
//            $file_list[$i]['filepath'] = $this->get_image_path($file_list[$i]['movie_id']) . $file_list[$i]['filename'];
//            // $file_list[$i]['annotations'] = M("annotation")->where('filename = \'' . $file_list[$i]['filename'] . '\'')->select();
//        }
//        echo json_encode($file_list);
    }

    public function get_pending($class_id, $round = 0)
    {
        $file_list = M("annotation")->distinct(true)->field('id,frame_index,filename,movie_id,w,h,x1,y1,x2,y2,confidence,round')
            ->where('class_id=' . $class_id . ' and (round='.$round.' or round=-'.$round.')')->order('round,frame_index')->select();
        for ($i = 0; $i < count($file_list); $i++) {
            $file_list[$i]['filepath'] = $this->get_image_path($file_list[$i]['movie_id']) . $file_list[$i]['filename'];
        }

        $ret['annotations'] = $file_list;
        $ret['total'] = count($file_list);


        $confirm_list = M("annotation")->distinct(true)->field('id, filename, movie_id, w, h, x1, y1, x2, y2')
            ->where('class_id=' . $class_id . ' and round=-' . $round)->order('filename')->select();

        $ret['count'] = count($confirm_list);

        echo json_encode($ret);
    }

    public function get_edit_frame($id)
    {
        $frame = M("frames")->where('id=\'' . $id . '\'')->find();
        $annotation = M("annotation")->where('frame_id=\'' . $id . '\'')->select();
        for ($i = 0; $i < count($annotation); $i++) {
            $movie = M("movie")->where('id=' . $annotation[$i]['movie_id'])->find();
            $classes = M("classes")->where('id=' . $annotation[$i]['class_id'])->find();
            $annotation[$i]['class_name'] = $classes['name'];
            $annotation[$i]['movie'] = $movie;
        }
        $ret['classes'] = M("classes")->select();
        $ret['annotation'] = $annotation;
        $ret['frame'] = $frame;
        echo json_encode($ret);
    }


    public function getf($id)
    {
        $Model = new \Think\Model();
//        $sql = 'SELECT annotation.*, movie.table_name  FROM annotation JOIN movie ON movie.id = annotation.movie_id WHERE annotation.id NOT IN (SELECT annotation_id AS id FROM training_annotation) AND annotation.movie_id = $movie_id AND annotation.class_id = $class_id AND round < 0 ORDER BY rand() LIMIT $total';
        $sql = 'SELECT annotation.*, movie.table_name, movie.name as movie_name  FROM annotation JOIN movie ON movie.id = annotation.movie_id WHERE annotation.id = $id AND round < 0 ORDER BY frame_index';
        $sql = str_replace('$id', $id, $sql);
        $annotation = $Model->query($sql);

//        $annotation = M("annotation")->where('id=\'' . $id . '\'')->select();
        for ($i = 0; $i < count($annotation); $i++) {
            $classes = M("classes")->where('id=' . $annotation[$i]['class_id'])->find();
            $annotation[$i]['class_name'] = $classes['name'];
        }
        $ret['classes'] = M("classes")->select();
        $ret['annotation'] = $annotation;
        echo json_encode($ret);
    }

    public function get_image_path($movie_id)
    {
        $movie = M("movie")->where('id=' . $movie_id)->find();
        return C('IMAGE_PATH') . $movie['table_name'] . '/all_frame/';
    }

    public function get_by_class($class_id = 0)
    {
        $annotation = M("annotation")
            ->where('class_id=' . $class_id)->select();
        for ($i = 0; $i < count($annotation); $i++) {
            $movie = M("movie")->where('id=' . $annotation[$i]['movie_id'])->find();
            $classes = M("classes")->where('id=' . $annotation[$i]['class_id'])->find();
            $movie['image_path'] = C('IMAGE_PATH') . $movie['table_name'] . '/all_frame/';
            $annotation[$i]['movie'] = $movie;
            $annotation[$i]['class_name'] = $classes['name'];
        }
        $ret['classes'] = M("classes")->select();
        $ret['annotation'] = $annotation;
        echo json_encode($ret);
    }

    public function get($annotation_id = 0)
    {
        $annotation = M("annotation")->where('id=' . $annotation_id)->find();
        $movie = M("movie")->where('id=' . $annotation['movie_id'])->find();
        $movie['image_path'] = C('IMAGE_PATH') . $movie['table_name'] . '/all_frame/';
        $annotation['movie'] = $movie;
        $annotation['classes'] = M("classes")->select();
        echo json_encode($annotation);
    }

    public function delete()
    {
        C('SHOW_PAGE_TRACE', false);
        if (!IS_POST) {
            return false;
        }
        if (IS_POST) {
            $input = json_decode(file_get_contents("php://input"), true);
            $annotation = M("annotation");
            $annotation->where('id=' . $input['id'])->delete();
            echo $annotation->getLastSql();
        }
    }

    private function _save($input)
    {
        if ($input['id'] == -1) {
            unset($input['id']);
            if ($input['x1'] == 0 && $input['y1'] == 0 && $input['x2'] == 0 && $input['y2'] == 0) {
                return;
            } else {
                $input['round'] = -1;
                $annotation = M("annotation");
                $annotation->field('filename,class_id,frame_id,movie_id,w,h,x1,y1,x2,y2,round')->add($input);
            }
        } else {
            $annotation = M("annotation");
            $annotation->where('id=%d', $input['id'])->field('id,filename,class_id,frame_id,movie_id,w,h,x1,y1,x2,y2,round')->save($input);
        }
        #echo $annotation->getLastSql();
    }

    public function save()
    {
        C('SHOW_PAGE_TRACE', false);
        if (!IS_POST) {
            return false;
        }
        if (IS_POST) {
            $input = json_decode(file_get_contents("php://input"), true);
            for ($i = 0; $i < count($input); $i++) {
                $this->_save($input[$i]);
            }

            $confirm_list = M("annotation")->distinct(true)->field('id,filename,movie_id,w,h,x1,y1,x2,y2,round')
                ->where('class_id=' . $input[0]['class_id'] . ' and round=6')->order('filename')->select();

            echo count($confirm_list);
        }

    }

    public function getframes($class_id = 0, $minute = 0)
    {
        $class = M("classes")->where('id=' . $class_id)->find();
        $Movie = M("movie")->where('id=' . $class['movie_id'])->find();
        if (count($Movie) > 0) {

            $Frames = M('frames')->where('movie_id=' . $Movie('id'));
            $count = $Frames->count();
            $pages = array();
            if ($minute > 0) {
                $data = array();
                $data['name'] = 'Prev';
                $data['link'] = '/ai/annotation/create/class_id/' . $class_id . '/minute/' . ($minute - 1);
                array_push($pages, $data);
            }

            $start = ($minute - 9) > 0 ? ($minute - 9) : 0;

            for ($i = $start; $i < ($count / (24 * 60)) + 1; $i++) {
                $data = array();
                $data['name'] = $i;
                if ($i == $minute) {
                    $data['class'] = 'page-item active';
                } else {
                    $data['class'] = 'page-item';
                }

                $data['link'] = '/ai/annotation/create/class_id/' . $class_id . '/minute/' . $i;
                array_push($pages, $data);
                if (count($pages) > 19) {
                    break;
                }
            }

            if ($minute < ($count / (24 * 60))) {
                $data = array();
                $data['name'] = 'Next';
                $data['class'] = 'page-item';
                $data['link'] = '/ai/annotation/create/class_id/' . $class_id . '/minute/' . ($minute + 1);
                array_push($pages, $data);
            }

            $frames = $Frames->limit($minute * 24 * 60, 24 * 60)->select();
            $ret['count'] = $count;
            $ret['pages'] = $pages;
            $ret['frames'] = $frames;
            $ret['minute'] = $minute;
            $ret['movie_id'] = $class['movie_id'];
            $ret['classes'] = M("classes")->select();
            $ret['image_path'] = C('IMAGE_PATH') . $Movie['table_name'] . '/all_frame/';
            echo json_encode($ret);
        }
        return;
    }

    public function getkeyframes($class_id = 0)
    {
        $class = M("classes")->where('id=' . $class_id)->find();
        $movie = M("movie")->where('id=' . $class['movie_id'])->find();
        $frames = M("frames")->where('movie_id=' . $class['movie_id'] . ' and keyframe=1')->select();
        $ret['count'] = count($frames);
        $ret['frames'] = $frames;
        $ret['movie_id'] = $class['movie_id'];
        $ret['classes'] = M("classes")->select();
        $ret['image_path'] = C('IMAGE_PATH') . $movie['table_name'] . '/all_frame/';
        $ret['thumb_path'] = C('IMAGE_PATH') . $movie['table_name'] . '/thumb_frame/';
        echo json_encode($ret);
    }


    public function get_annotation_list($class_id = 0)
    {
        $annotation = M("annotation")->where("class_id='" . $class_id . "'")->select();
        echo json_encode($annotation);
    }

    public function new_annotation($class_id, $frame_id, $filename)
    {
        $class = M("classes")->where('id=' . $class_id)->find();
        $annotation['id'] = -1;
        $annotation['frame_id'] = $frame_id;
        $annotation['class_id'] = $class['id'];
        $annotation['class_name'] = $class['name'];
        $annotation['movie_id'] = $class['movie_id'];
        $movie = M("movie")->where('id=' . $class['movie_id'])->find();
        $annotation['w'] = $movie['w'];
        $annotation['h'] = $movie['h'];
        $annotation['x1'] = '0';
        $annotation['y1'] = '0';
        $annotation['x2'] = '0';
        $annotation['y2'] = '0';
        $annotation['filename'] = $filename;
        $annotation['movie'] = $movie;
        echo json_encode($annotation);
    }

    public function add_one_training_data($movie_id = 0, $class_id = 0, $total = 1)
    {
        $Model = new \Think\Model();
//        $sql = 'SELECT annotation.*, movie.table_name  FROM annotation JOIN movie ON movie.id = annotation.movie_id WHERE annotation.id NOT IN (SELECT annotation_id AS id FROM training_annotation) AND annotation.movie_id = $movie_id AND annotation.class_id = $class_id AND round < 0 ORDER BY rand() LIMIT $total';
        $sql = 'SELECT results.* FROM results JOIN movie ON movie.id = results.movie_id WHERE results.movie_id = $movie_id AND results.class_id = $class_id AND confidence > 0.2 ORDER BY rand() LIMIT $total';
        $sql = str_replace('$movie_id', $movie_id, $sql);
        $sql = str_replace('$class_id', $class_id, $sql);
        $sql = str_replace('$total', $total, $sql);
        $ret = $Model->query($sql);
        $new['data'] = $ret[0];

        $sql = "select annotation.*, movie.table_name from annotation join movie on movie.id = annotation.movie_id where annotation.frame_id < " . $new['data']['frame_id'] . " order by annotation.frame_id desc limit 1";
        $ret = $Model->query($sql);
        $new['prev'] = $ret[0];

        $sql = "select annotation.*, movie.table_name from annotation join movie on movie.id = annotation.movie_id where annotation.frame_id > " . $new['data']['frame_id'] . " order by annotation.frame_id limit 1";
        $ret = $Model->query($sql);
        $new['next'] = $ret[0];

        echo json_encode($new);
    }


}
