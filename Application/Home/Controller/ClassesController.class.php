<?php

namespace Home\Controller;

use Think\Controller;

class ClassesController extends Controller
{
    public function index()
    {
        $this->display();
    }

    public function movie($movie_id)
    {
        $this->assign('movie_id', $movie_id);
        $movie = M("movie")->where('id=' . $movie_id)->find();
        $this->assign('movie_name', $movie['name']);
        $this->display();
    }

    public function add($movie_id)
    {
        $this->assign('movie_id', $movie_id);
        $this->display();
    }

    public function edit($class_id)
    {
        $this->assign('class_id', $class_id);
        $this->display();
    }

    public function ground_truth($class_id, $weights_id = 0, $minute = 0)
    {
        $class = M("classes")->where('id=' . $class_id)->find();
        $this->assign('class_id', $class['id']);
        $this->assign('weights_id', $weights_id);
        $this->assign('movie_id', $class['movie_id']);
        $this->assign('minute', $minute);
        $this->display();
    }

    public function get_pending_round_count($movie_id, $class_id = 0, $round){
        $Model = new \Think\Model();
        $sql = "select count(*)  from annotation where movie_id=" . $movie_id . " and class_id=" . $class_id . " and round =" . $round;
        $ret = $Model->query($sql);
        return $ret[0]['count(*)'];
    }

    public function get_pending_round_list($movie_id, $class_id = 0)
    {
        $Model = new \Think\Model();
        $sql = "select DISTINCT round  from annotation where movie_id=" . $movie_id . " and class_id=" . $class_id . " and round > 0";
        $ret = $Model->query($sql);
        for ($i = 0; $i < count($ret); $i++) {
            $ret[$i]['count'] = $this->get_pending_round_count($movie_id, $class_id, $ret[$i]['round']);
        }
        return $ret;
    }

    public function getmovies()
    {
        $movies = M("movie")->select();
        for ($i = 0; $i < count($movies); $i++) {
            $class = M("classes")->where('movie_id=' . $movies[$i]['id'])->select();
            $movies[$i]['classes'] = $class;
//            $movies[$i]['image'] = C('IMAGE_PATH') . 'movie_covers/' . $movies[$i]['image'];
        }
        echo json_encode($movies);
    }

    public function get($class_id)
    {
        $class = M("classes")->where('id=' . $class_id)->find();
        $movie = M("movie")->where('id=' . $class['movie_id'])->find();
        $movies = M("movie")->select();
        $ret['movies'] = $movies;
        $ret['class'] = $class;
        $modal['image_path'] = C('IMAGE_PATH') . $movie['table_name'] . '/all_frame/';
        $ret['modal'] = $modal;
        echo json_encode($ret);
    }

    public function create($movie_id)
    {
        $movie = M('movie')->select();
        $ret['movies'] = $movie;
        $class['movie_id'] = $movie_id;
        $class['id'] = -1;
        $class['name'] = 'new class';
        $class['image'] = '';
        $ret['class'] = $class;
        echo json_encode($ret);
    }

    public function get_key_frames($movie_id)
    {
        $movie = M("movie")->where('id=' . $movie_id)->find();
        if (count($movie) > 0) {
            $frames = M('frames')->where('movie_id=' . $movie_id . ' and keyframe=1')->select();
            $ret['total'] = count($frames);
            $ret['frames'] = $frames;
            $ret['movie'] = $movie;
            $ret['image_path'] = C('IMAGE_PATH') . $movie['table_name'] . '/all_frame/';
            echo json_encode($ret);
        }
        return;
    }

    public function all($movie_id)
    {
        $Classes = M("classes")->where("movie_id=" . $movie_id)->select();
        for ($i = 0; $i < count($Classes); $i++) {
            $count = M("annotation")->where('class_id=' . $Classes[$i]['id'] . ' and (round<1)')->select();
            $Classes[$i]['count'] = count($count);
            $Classes[$i]['pending_round_list'] = $this->get_pending_round_list($movie_id, $Classes[$i]['id']);
            $movie = M('movie')->where('id=' . $Classes[$i]['movie_id'])->find();
            $Classes[$i]['image_path'] = C('IMAGE_PATH') . $movie['table_name'] . '/all_frame/';
        }
        echo json_encode($Classes);
    }

    private function _save($input)
    {
        if ($input['id'] == -1) {
            unset($input['id']);
            $classes = M("classes");
            $classes->add($input);
        } else {
            $classes = M("classes");
            $classes->where('id=%d', $input['id'])->save($input);
        }
        echo $classes->getLastSql();
    }

    public function save()
    {
        C('SHOW_PAGE_TRACE', false);
        if (!IS_POST) {
            return false;
        }
        if (IS_POST) {
            $input = json_decode(file_get_contents("php://input"), true);
            $this->_save($input);
        }
    }

    public function save_gt()
    {
        C('SHOW_PAGE_TRACE', false);
        if (!IS_POST) {
            return false;
        }
        if (IS_POST) {
            $input = json_decode(file_get_contents("php://input"), true);
            $ground_truth = M("ground_truth");
            if ($input['flag'] == 0) {
                $ground_truth->where('filename=\'' . $input['filename'] . '\'')->delete();
            } else {
                $gt = $ground_truth->where('filename=\'' . $input['filename'] . '\'')->find();
                if ($gt != null) {
                    $ground_truth->where('filename=\'' . $input['filename'] . '\'')
                        ->field('class_id,movie_id,frame_id,flag,filename')
                        ->save($input);
                } else {
                    $ground_truth->field('class_id,movie_id,frame_id,flag,filename')->add($input);
                }
            }
            echo $ground_truth->getLastSql();
//            echo json_encode($gt);
        }
    }

    public function get_avaliable_weights($movie_id = 0){
        $Model = new \Think\Model();
        $sql = 'select DISTINCT results.weights_id, weights.name from results
left join weights on weights.id = results.weights_id
where results.movie_id =' . $movie_id;
        $ret = $Model->query($sql);
        echo json_encode($ret);
    }

    public function get_gt($movie_id = 0)
    {
        $ground_truth = M("ground_truth")->where("movie_id=" . $movie_id)->order('frame_id')->select();
        echo json_encode($ground_truth);
    }

    public function is_gt($frame_id = 0)
    {
        $ret = 0;
        $frame = M("frames")->where("id=" . $frame_id)->find();
        $movie_id = $frame['movie_id'];
        $ground_truth = M("ground_truth")->where('movie_id=' . $movie_id . ' and frame_id <= ' . $frame_id)->order('frame_id desc')->find();
        if ($ground_truth['frame_id'] == $frame_id){
            $ret = 1;
        }
        else if (count($ground_truth) > 0 && $ground_truth['flag'] == 1){
            $gt = M("ground_truth")->where('movie_id=' . $movie_id . ' and frame_id >= ' . $frame_id)->order('frame_id')->find();
            if (count($gt) > 0 && $gt['flag'] == 2){
                $ret = 1;
            }
        }
        echo $ret;
    }

    public function get_ground_truth_data($class_id = 0, $weights_id = 1, $minute = 0)
    {
        $class = M("classes")->where('id=' . $class_id)->find();
        $Movie = M("movie")->where('id=' . $class['movie_id'])->find();
        $movie_id = $class['movie_id'];

        if (count($Movie) > 0) {

            $Model = new \Think\Model();
            $sql = "SELECT count(*) FROM frames LEFT JOIN results ON results.frame_id = frames.id AND results.weights_id = " . $weights_id . " WHERE frames.movie_id = " . $movie_id;
//            echo $sql;
            $cnt_ret = $Model->query($sql);

//            $Frames = M('frames')->where('movie_id=' . $class['movie_id']);
            $count = $cnt_ret[0]['count(*)'];

            $pages = array();
            if ($minute > 0) {
                $data = array();
                $data['name'] = 'Prev';
                $data['link'] = '/ai/classes/ground_truth/class_id/' . $class_id . '/weights_id/' . $weights_id . '/minute/' . ($minute - 1);
                array_push($pages, $data);
            }

            $start = ($minute - 9) > 0 ? ($minute - 9) : 0;

            for ($i = $start; $i < ($count / (24 * 60)) + 1; $i++) {
                $data = array();
                $data['name'] = $i;
                if ($i == $minute) {
                    $data['class'] = 'page-item active';
                } else {
                    $data['class'] = 'page-item';
                }

                $data['link'] = '/ai/classes/ground_truth/class_id/' . $class_id . '/weights_id/' . $weights_id . '/minute/' . $i;
                array_push($pages, $data);
                if (count($pages) > 19) {
                    break;
                }
            }

            if ($minute < ($count / (24 * 60))) {
                $data = array();
                $data['name'] = 'Next';
                $data['class'] = 'page-item';
                $data['link'] = '/ai/classes/ground_truth/class_id/' . $class_id  . '/weights_id/' . $weights_id  . '/minute/' . ($minute + 1);
                array_push($pages, $data);
            }

            $sql = 'SELECT frames.id, frames.movie_id, frames.filename, movie.w, movie.h, results.x1, results.y1, results.x2, results.y2, results.confidence FROM frames LEFT JOIN results ON results.frame_id = frames.id AND results.weights_id = $weights_id left join movie on movie.id = results.movie_id WHERE frames.movie_id = $movie_id LIMIT $minute, 1440';
            $sql = str_replace('$movie_id', $movie_id, $sql);
            $sql = str_replace('$weights_id', $weights_id, $sql);
            $sql = str_replace('$minute', $minute * 24 * 60, $sql);

            $frames = $Model->query($sql);

//            $frames = $Frames->where('movie_id=' . $class['movie_id'])->limit($minute * 24 * 60, 24 * 60)->select();
            $ret['count'] = $count;
            $ret['pages'] = $pages;
            $ret['frames'] = $frames;
            $ret['minute'] = $minute;
            $ret['movieid'] = $class['movie_id'];
            $ret['image_path'] = C('IMAGE_PATH') . $Movie['table_name'] . '/all_frame/';
            echo json_encode($ret);
        }
        return;
    }

}
