<?php

namespace Home\Controller;

use Think\Controller;

class MoviesController extends Controller
{
    public function index()
    {
        $this->display();
    }

    public function frames($movie_id = 0, $minute = 0)
    {
        $movies = M("movie")->where('id=' . $movie_id)->find();
        if (count($movies) > 0) {
            $this->assign('name', $movies['name']);
            $this->assign('movie_id', $movie_id);
            $this->assign('minute', $minute);
        }
        $this->display();
    }

    public function keyframes($movie_id = 0)
    {
        $movies = M("movie")->where('id=' . $movie_id)->find();
        if (count($movies) > 0) {
            $this->assign('name', $movies['name']);
            $this->assign('movie_id', $movie_id);
        }
        $this->display();
    }

    public function faceframes($movie_id = 0)
    {
        $movies = M("movie")->where('id=' . $movie_id)->find();
        if (count($movies) > 0) {
            $this->assign('name', $movies['name']);
            $this->assign('movie_id', $movie_id);
        }
        $this->display();
    }

    public function facessdframes($movie_id = 0, $minute = 0)
    {
        $movies = M("movie")->where('id=' . $movie_id)->find();
        if (count($movies) > 0) {
            $this->assign('name', $movies['name']);
            $this->assign('movie_id', $movie_id);
            $this->assign('minute', $minute);
        }

        $this->display();
    }

    public function get_pages($movieid, $minute, $count)
    {
        $pages = array();
        if ($minute > 0) {
            $data = array();
            $data['name'] = 'Prev';
            $data['link'] = '/ai/movies/frames/movieid/' . $movieid . '/minute/' . ($minute - 1);
            array_push($pages, $data);
        }

        $start = ($minute - 9) > 0 ? ($minute - 9) : 0;

        for ($i = $start; $i < ($count / (24 * 60)) + 1; $i++) {
            $data = array();
            $data['name'] = $i;
            if ($i == $minute) {
                $data['class'] = 'page-item active';
            } else {
                $data['class'] = 'page-item';
            }

            $data['link'] = '/ai/movies/frames/movieid/' . $movieid . '/minute/' . $i;
            array_push($pages, $data);
            if (count($pages) > 19) {
                break;
            }
        }

        if ($minute < ($count / (24 * 60))) {
            $data = array();
            $data['name'] = 'Next';
            $data['class'] = 'page-item';
            $data['link'] = '/ai/movies/frames/movieid/' . $movieid . '/minute/' . ($minute + 1);
            array_push($pages, $data);
        }
        return $pages;
    }

    public function get_ssd_pages($movie_id, $minute, $count)
    {
        $pages = array();
        if ($minute > 0) {
            $data = array();
            $data['name'] = 'Prev';
            $data['link'] = '/ai/movies/facessdframes/movie_id/' . $movie_id . '/minute/' . ($minute - 1);
            array_push($pages, $data);
        }

        $start = ($minute - 9) > 0 ? ($minute - 9) : 0;

        for ($i = $start; $i < ($count / (24 * 60)) + 1; $i++) {
            $data = array();
            $data['name'] = $i;
            if ($i == $minute) {
                $data['class'] = 'page-item active';
            } else {
                $data['class'] = 'page-item';
            }

            $data['link'] = '/ai/movies/facessdframes/movie_id/' . $movie_id . '/minute/' . $i;
            array_push($pages, $data);
            if (count($pages) > 19) {
                break;
            }
        }

        if ($minute < ($count / (24 * 60))) {
            $data = array();
            $data['name'] = 'Next';
            $data['class'] = 'page-item';
            $data['link'] = '/ai/movies/facessdframes/movie_id/' . $movie_id . '/minute/' . ($minute + 1);
            array_push($pages, $data);
        }
        return $pages;
    }

    public function get_frames($movie_id = 0, $minute = 0)
    {
        $movies = M("movie")->where('id=' . $movie_id)->find();
        if (count($movies) > 0) {

            $Frames = M('frames')->where('movie_id=' . $movie_id);
            $count = $Frames->count();

            $frames = $Frames->limit($minute * 24 * 60, 24 * 60)->select();
            $ret['count'] = $count;
            $ret['pages'] = $this->get_pages($movie_id, $minute, $count);
            $ret['frames'] = $frames;
            $ret['minute'] = $minute;
            $ret['movie_id'] = $movie_id;
            $ret['image_path'] = C('IMAGE_PATH') . $movies['table_name'] . '/thumb_frame/';
            echo json_encode($ret);
        }
        return;
    }

    public function get_key_frames($movie_id)
    {
        $movies = M("movie")->where('id=' . $movie_id)->find();
        if (count($movies) > 0) {
            $frames = M('frames')->where('movie_id=' . $movie_id . ' and keyframe=1')->select();
            $ret['total'] = count($frames);
            $ret['frames'] = $frames;
            $ret['movieid'] = $movies;
            $ret['image_path'] = C('IMAGE_PATH') . $movies['table_name'] . '/thumb_frame/';
            echo json_encode($ret);
        }
        return;
    }

    public function get_face_frames($movie_id)
    {
        $movies = M("movie")->where('id=' . $movie_id)->find();
        if (count($movies) > 0) {
            $frames = M('frames')->where('movie_id=' . $movie_id . ' and keyframe=1 and faces>0')->select();
            $ret['total'] = count($frames);
            $ret['frames'] = $frames;
            $ret['movie_id'] = $movie_id;
            $ret['image_path'] = C('IMAGE_PATH') . $movies['table_name'] . '/thumb_frame/';
            echo json_encode($ret);
        }
        return;
    }

    public function get_face_ssd_frames($movie_id = 0, $minute = 0)
    {
        $movies = M("movie")->where('id=' . $movie_id)->find();
        if (count($movies) > 0) {

            $Model = new \Think\Model();
            $sql = "select count(*) from frames_faces 
left join frames on frames.id = frames_faces.frame_id
left join movie on movie.id = frames_faces.movie_id
where frames_faces.movie_id = $movie_id ";

            $sql = str_replace('$movie_id', $movie_id, $sql);

            $ret = $Model->query($sql);

            $count = $ret[0]['count(*)'];

            $sql = "select frames.id, frames.filename, frames.time, frames_faces.x1, frames_faces.y1, frames_faces.x2, frames_faces.y2, movie.w, movie.h from frames_faces 
left join frames on frames.id = frames_faces.frame_id
left join movie on movie.id = frames_faces.movie_id
where frames_faces.movie_id = $movie_id limit minute,1440";

            $limit = $minute*1440;

            $sql = str_replace('$movie_id', $movie_id, $sql);
            $sql = str_replace('minute', $limit, $sql);
            $Model1 = new \Think\Model();
            $frames = $Model1->query($sql);

            $ret['limit'] = $limit;
            $ret['count'] = $count;
            $ret['pages'] = $this->get_ssd_pages($movie_id, $minute, $count);
            $ret['frames'] = $frames;
            $ret['minute'] = $minute;
            $ret['movie_id'] = $movie_id;
            $ret['image_path'] = C('IMAGE_PATH') . $movies['table_name'] . '/all_frame/';
            echo json_encode($ret);
        }
        return;

    }

    public function get_annotation_cnt($class_id){
        $Model = new \Think\Model();
        $sql = "select count(*) from annotation where round<0 and class_id = $class_id ";

        $sql = str_replace('$class_id', $class_id, $sql);

        $ret = $Model->query($sql);

        $count = $ret[0]['count(*)'];

        return $count;
    }


    public function get_movies()
    {
        $movies = M("movie")->select();
        for ($i = 0; $i < count($movies); $i++) {
            $class = M("classes")->where('movie_id=' . $movies[$i]['id'])->select();
            if (count($class) > 0)
                $class[0]['annotation_cnt'] = $this->get_annotation_cnt($class[0]['id']);
            $movies[$i]['classes'] = $class;
        }
        $ret['total'] = count($movies);
        $ret['movies'] = $movies;
        echo json_encode($ret);
    }

    public function create_frames($movie_id = 0)
    {
        $movies = M("movie")->where('id=' . $movie_id)->find();
        // echo exec('whoami');

        // $cmd1 = 'rm -rf ../projects/' . $movies['table_name'] . '/all_frame';
        // exec($cmd1);
        // $cmd2 = 'mkdir ../projects/' . $movies['table_name'];
        // exec($cmd2);
        // $cmd3 = 'mkdir ../projects/' . $movies['table_name'] . '/all_frame';
        // exec($cmd3);

        //
        $cmd = 'ffmpeg -i ./movies/';
        $cmd = $cmd . $movies['filename'];
        $cmd = $cmd . ' -s 720*405 -qscale:v 2 ./';
        $cmd = $cmd . $movies['table_name'];
        $cmd = $cmd . '/all_frame/';
        $cmd = $cmd . $movies['prefix'];
        $cmd = $cmd . '%06d.JPEG';


        $cmd4 = "cursor.execute(\"insert into frames ";
        $cmd4 = $cmd4 . " (movie_id, filename, time) VALUES ('" . $movies['id'] . ", `";
        $cmd4 = $cmd4 . $movies['prefix'];
        $cmd4 = $cmd4 . "\" + str(x).zfill(6) + \".JPEG', \" + str(x*(1000/24)) + \")\")";

        //
        // $ret = shell_exec($cmd);


        //$a = shell_exec("python ../GT/convert_tensorflow.py");
        echo $cmd;
        echo "<br/>";
        echo $cmd4;

    }
}
