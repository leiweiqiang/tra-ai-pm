<?php

namespace Home\Controller;

use Think\Controller;

class ModelsController extends Controller
{
    public function index()
    {
        $this->display();
    }

    public function detail($model_id = 0, $class_id)
    {
        $model = M("model")->where('id=' . $model_id)->find();
        $this->assign('model_id', $model['id']);
        $this->assign('model_name', $model['name']);
        $this->assign('class_id', $class_id);
        $this->display();
    }

    public function testing_data($model_id = 0, $movie_id = 0, $class_id = 0)
    {
        $model = M("model")->where('id=' . $model_id)->find();
        $this->assign('model_id', $model['id']);
        $this->assign('model_name', $model['name']);
        $this->assign('movie_id', $movie_id);
        $this->assign('class_id', $class_id);
        $this->display();
    }

    public function get_testing_data($model_id = 0, $movie_id = 0)
    {
        $sql = 'SELECT annotation.*, movie.table_name FROM testing_annotation
JOIN annotation ON annotation.id = testing_annotation.annotation_id
JOIN movie ON annotation.movie_id = movie.id
WHERE 
(annotation.movie_id = $movie_id)
OR (model_id = 0 AND annotation.movie_id = 2)
ORDER BY annotation.frame_index';
        $sql = str_replace('$model_id', $model_id, $sql);
        $sql = str_replace('$movie_id', $movie_id, $sql);

        $Model = new \Think\Model();
        $ret = $Model->query($sql);

        echo json_encode($ret);

    }


    public function training_data($model_id = 0, $movie_id = 0, $class_id = 0)
    {
        $model = M("model")->where('id=' . $model_id)->find();
        $this->assign('model_id', $model['id']);
        $this->assign('model_name', $model['name']);
        $this->assign('movie_id', $movie_id);
        $this->assign('class_id', $class_id);
        $this->display();
    }

    public function get_training_data($model_id = 0, $movie_id = 0)
    {
        $sql = 'SELECT annotation.*, movie.table_name FROM training_annotation
JOIN annotation ON annotation.id = training_annotation.annotation_id
JOIN movie ON annotation.movie_id = movie.id
WHERE model_id = $model_id
AND annotation.movie_id = $movie_id
ORDER BY annotation.frame_id';
        $sql = str_replace('$model_id', $model_id, $sql);
        $sql = str_replace('$movie_id', $movie_id, $sql);

        $Model = new \Think\Model();
        $ret = $Model->query($sql);

        echo json_encode($ret);

    }

    public function get_iou($result)
    {

        $x1 = $result['x1'];
        $y1 = $result['y1'];
        $x2 = $result['x2'];
        $y2 = $result['y2'];

        $sql = 'SELECT annotation.* FROM testing_annotation
JOIN annotation ON annotation.id = testing_annotation.annotation_id
WHERE annotation.frame_id =' . $result['frame_id'];

        $Model = new \Think\Model();
        $ret = $Model->query($sql);

        $_x1 = $ret[0]['x1'];
        $_y1 = $ret[0]['y1'];
        $_x2 = $ret[0]['x2'];
        $_y2 = $ret[0]['y2'];

        $areai = ($x2 - $x1 + 1) * ($y2 - $y1 + 1);
        $areaj = ($_x2 - $_x1 + 1) * ($_y2 - $_y1 + 1);

        $xx1 = max($x1, $_x1);
        $yy1 = max($y1, $_y1);
        $xx2 = min($x2, $_x2);
        $yy2 = min($y2, $_y2);

        $h = max(0, $yy2 - $yy1 + 1);
        $w = max(0, $xx2 - $xx1 + 1);

        $intersection = $w * $h;

        $iou = $intersection / ($areai + $areaj - $intersection);

        return $iou;
    }

    public function add_training_data($model_id = 0, $movie_id = 0, $class_id = 0, $total = 0)
    {
        $Model = new \Think\Model();
        $sql = 'SELECT id FROM annotation WHERE id NOT IN (SELECT annotation_id AS id FROM training_annotation) AND annotation.movie_id = $movie_id AND annotation.class_id = $class_id AND round < 0 ORDER BY rand() LIMIT $total';
        $sql = str_replace('$movie_id', $movie_id, $sql);
        $sql = str_replace('$class_id', $class_id, $sql);
        $sql = str_replace('$total', $total, $sql);
        $ret = $Model->query($sql);

        for ($i = 0; $i < count($ret); $i++) {
            $data['annotation_id'] = intval($ret[$i]['id']);
            $data['model_id'] = intval($model_id);
            $training_annotation = M("training_annotation");
            $train = $training_annotation->add($data);
        }
        echo json_encode($sql);
    }

    public function add_one_training_data_save($model_id = 0, $annotation_id = 0)
    {
        $data['annotation_id'] = intval($annotation_id);
        $data['model_id'] = intval($model_id);
        $training_annotation = M("training_annotation");
        $train = $training_annotation->add($data);
        echo json_encode($train);
    }

    public function add_one_training_data($model_id = 0, $movie_id = 0, $class_id = 0, $total = 1)
    {
        $Model = new \Think\Model();
        $sql = 'SELECT annotation.*, movie.table_name  FROM annotation JOIN movie ON movie.id = annotation.movie_id WHERE annotation.id NOT IN (SELECT annotation_id AS id FROM training_annotation) AND annotation.movie_id = $movie_id AND annotation.class_id = $class_id AND round < 0 ORDER BY rand() LIMIT $total';
        $sql = str_replace('$movie_id', $movie_id, $sql);
        $sql = str_replace('$class_id', $class_id, $sql);
        $sql = str_replace('$total', $total, $sql);
        $ret = $Model->query($sql);
        $new['data'] = $ret[0];

        $sql = "select annotation.*, movie.table_name from annotation join training_annotation on annotation.id = training_annotation.annotation_id join movie on movie.id = annotation.movie_id where training_annotation.model_id = $model_id and annotation.frame_id < " . $new['data']['frame_id'] . " order by annotation.frame_id desc limit 1";
        $sql = str_replace('$model_id', $model_id, $sql);
        $ret = $Model->query($sql);
        $new['prev'] = $ret[0];

        $sql = "select annotation.*, movie.table_name from annotation join training_annotation on annotation.id = training_annotation.annotation_id join movie on movie.id = annotation.movie_id where training_annotation.model_id = $model_id and annotation.frame_id > " . $new['data']['frame_id'] . " order by annotation.frame_id limit 1";
        $sql = str_replace('$model_id', $model_id, $sql);
        $ret = $Model->query($sql);
        $new['next'] = $ret[0];

        echo json_encode($new);
    }

    public function add_testing_data($model_id = 0, $movie_id = 0, $class_id = 0, $total = 0)
    {
        $Model = new \Think\Model();
        $sql = 'SELECT id FROM annotation WHERE id NOT IN (SELECT annotation_id AS id FROM testing_annotation) AND annotation.movie_id = $movie_id AND annotation.class_id = $class_id AND round < 0 ORDER BY rand() LIMIT $total';
        $sql = str_replace('$movie_id', $movie_id, $sql);
        $sql = str_replace('$class_id', $class_id, $sql);
        $sql = str_replace('$total', $total, $sql);
        $ret = $Model->query($sql);

        for ($i = 0; $i < count($ret); $i++) {
            $data['annotation_id'] = intval($ret[$i]['id']);
            $data['model_id'] = intval($model_id);
            $training_annotation = M("testing_annotation");
            $train = $training_annotation->add($data);
        }
        echo json_encode($sql);
    }

    public function classes($model_id = 0)
    {
        $model = M("model")->where('id=' . $model_id)->find();
        $this->assign('model_id', $model['id']);
        $this->assign('model_name', $model['name']);
        $this->display();
    }

    public function model($model_id = 0)
    {
        $model = M("model")->where('id=' . $model_id)->find();
        $this->assign('model_id', $model['id']);
        $this->assign('model_name', $model['name']);
        $this->display();
    }

    public function weights($model_id = 0)
    {
        $model = M("model")->where('id=' . $model_id)->find();
        $this->assign('model_id', $model['id']);
        $this->assign('model_name', $model['name']);
        $this->display();
    }

    public function edit($model_id = 0)
    {
        $model = M("model")->where('id=' . $model_id)->find();
        $this->assign('model_id', $model['id']);
        $this->assign('model_name', $model['name']);
        $this->display();
    }


    public function threshold($model_id = 0, $weights_id = 0, $class_id = 0)
    {
        $model = M("model")->where('id=' . $model_id)->find();
        $this->assign('model_id', $model['id']);
        $this->assign('weights_id', $weights_id);
        $this->assign('class_id', $class_id);
        $this->assign('model_name', $model['name']);
        $this->display();
    }


    public function get_threshold($model_id = 0, $weights_id = 0, $class_id = 0)
    {
        $data = array();
        $Model = new \Think\Model();
        for ($i = 2; $i < 10; $i++) {
            $sql = "SELECT count(*)
      FROM results
      WHERE model_id=" . $model_id . " AND weights_id=" . $weights_id . " AND class_id=" . $class_id . " AND confidence>" . $i * 0.1;

            $ret = $Model->query($sql);
            $count = $ret[0]['count(*)'];
            array_push($data, $count);
        }

        echo json_encode($data);
    }

    public function get_edit($model_id = 0)
    {
        $Model = new \Think\Model();
        $sql = 'SELECT  classes.id AS class_id, classes.name AS class_name, classes.movie_id, classes.image, model_classes.id, model_classes.model_id AS m_id, movie.name AS movie_name, movie.table_name, movie.image AS movie_image, movie.id AS movie_id  FROM classes LEFT OUTER JOIN model_classes ON model_classes.class_id = classes.id AND model_classes.model_id = ' . $model_id . ' LEFT OUTER JOIN movie ON movie.id=classes.movie_id ORDER BY classes.id';
        $ret = $Model->query($sql);
        echo json_encode($ret);
    }

    public function add_classes($model_id = 0, $class_id = 0)
    {
        $data['model_id'] = $model_id;
        $data['class_id'] = $class_id;
        $ret = M("model_classes")->add($data);
        echo json_encode($ret);
    }

    public function del_classes($model_id = 0, $class_id = 0)
    {
        $ret = M("model_classes")->where('model_id=' . $model_id . ' and class_id=' . $class_id)->delete();
        echo json_encode($ret);
    }


    public function get_weights($model_id = 0)
    {
        $ret = M("weights")->where('model_id=' . $model_id)->select();
        echo json_encode($ret);
    }

    public function get_pages($model_id, $movie_id, $class_id, $confidence = 0.2, $minute, $count)
    {
        $pages = array();
        if ($minute > 0) {
            $data = array();
            $data['name'] = 'Prev';
            $data['class'] = 'page-item';
            $data['link'] = '/ai/models/results/model_id/' . $model_id . '/movie_id/' . $movie_id . '/class_id/' . $class_id . '/confidence/' . $confidence . '/minute/' . ($minute - 1);
            array_push($pages, $data);
        }

        $start = ($minute - 9) > 0 ? ($minute - 9) : 0;

        for ($i = $start; $i < ($count / (24 * 60)) + 1; $i++) {
            $data = array();
            $data['name'] = $i;
            if ($i == $minute) {
                $data['class'] = 'page-item active';
            } else {
                $data['class'] = 'page-item';
            }

            $data['link'] = '/ai/models/results/model_id/' . $model_id . '/movie_id/' . $movie_id . '/class_id/' . $class_id . '/confidence/' . $confidence . '/minute/' . $i;
            array_push($pages, $data);
            if (count($pages) > 19) {
                break;
            }
        }

        if ($minute < ($count / (24 * 60))) {
            $data = array();
            $data['name'] = 'Next';
            $data['class'] = 'page-item';
            $data['link'] = '/ai/models/results/model_id/' . $model_id . '/movie_id/' . $movie_id . '/class_id/' . $class_id . '/confidence/' . $confidence . '/minute/' . ($minute + 1);
            array_push($pages, $data);
        }
        return $pages;
    }

    public function get_training_annotation_cnt($model_id = 0, $movie_id = 0)
    {
        $sql = 'SELECT count(*) FROM training_annotation
JOIN annotation ON annotation.id = training_annotation.annotation_id
JOIN movie ON annotation.movie_id = movie.id
WHERE model_id = $model_id
AND annotation.movie_id = $movie_id
ORDER BY annotation.frame_id';
        $sql = str_replace('$model_id', $model_id, $sql);
        $sql = str_replace('$movie_id', $movie_id, $sql);

        $Model = new \Think\Model();
        $ret = $Model->query($sql);
        $count = $ret[0]['count(*)'];
        return $count;
    }

    public function get_testing_annotation_cnt($model_id = 0, $movie_id = 0)
    {
        $sql = 'SELECT count(*) FROM testing_annotation
JOIN annotation ON annotation.id = testing_annotation.annotation_id
JOIN movie ON annotation.movie_id = movie.id
WHERE model_id = $model_id
AND annotation.movie_id = $movie_id
ORDER BY annotation.frame_id';
        $sql = str_replace('$model_id', $model_id, $sql);
        $sql = str_replace('$movie_id', $movie_id, $sql);

        $Model = new \Think\Model();
        $ret = $Model->query($sql);
        $count = $ret[0]['count(*)'];
        return $count;
    }

    public function delete_training_annotation()
    {
        C('SHOW_PAGE_TRACE', false);
        if (!IS_POST) {
            return false;
        }
        if (IS_POST) {
            $input = json_decode(file_get_contents("php://input"), true);
            $annotation = M("training_annotation");
            $annotation->where('annotation_id=' . $input['id'])->delete();
            echo $annotation->getLastSql();
        }
    }

    public function delete_testing_annotation()
    {
        C('SHOW_PAGE_TRACE', false);
        if (!IS_POST) {
            return false;
        }
        if (IS_POST) {
            $input = json_decode(file_get_contents("php://input"), true);
            $annotation = M("testing_annotation");
            $annotation->where('annotation_id=' . $input['id'])->delete();
            echo $annotation->getLastSql();
        }
    }

    public function get_classes($model_id = 0)
    {
        $Model = new \Think\Model();
        $sql = "SELECT
        model_classes.id,
        classes.movie_id,
        classes.id AS class_id,
        classes.name AS class_name,
        movie.name AS movie_name
      FROM model_classes
      LEFT JOIN classes
      ON classes.id = model_classes.class_id
      LEFT JOIN movie
      ON movie.id = classes.movie_id
      WHERE model_classes.model_id
      =" . $model_id;

        $ret = $Model->query($sql);

        for ($i = 0; $i < count($ret); $i++) {
            $ret[$i]['training_data_cnt'] = $this->get_training_annotation_cnt($model_id, $ret[$i]['movie_id']);
            $ret[$i]['testing_data_cnt'] = $this->get_testing_annotation_cnt($model_id, $ret[$i]['movie_id']);
        }

        echo json_encode($ret);

    }

    public function results($model_id, $movie_id, $class_id, $confidence = 0.2, $minute = 0)
    {
        $model = M("model")->where('id=' . $model_id)->find();
        $this->assign('model_id', $model['id']);
        $this->assign('model_name', $model['name']);
        $this->assign('movie_id', $movie_id);
        $this->assign('class_id', $class_id);
        $this->assign('confidence', $confidence);
        $class = M("classes")->where('id=' . $class_id)->find();
        $this->assign('class_name', $class['name']);
        $this->assign('minute', $minute);
        $this->display();
        // echo json_encode($model);
    }

    public function all()
    {
        $model = M("model")->select();
        echo json_encode($model);
    }

    public function package($model_id = 0)
    {
        // $a = shell_exec("python ./Python/make_model.py -m " . $model_id . " -o YOLO3");
        $a = exec("pwd");
        echo $a;
    }

    public function get_movies($model_id = 0)
    {
        $Model = new \Think\Model();
        $sql = 'SELECT classes.movie_id, movie.name FROM model_classes
LEFT JOIN classes ON model_classes.class_id = classes.id
LEFT JOIN movie ON classes.movie_id = movie.id
WHERE model_id = ' . $model_id;
        $ret = $Model->query($sql);
        echo json_encode($ret);
    }

    public function movies($model_id, $weights_id)
    {
        $model = M("model")->where('id=' . $model_id)->find();
        $weights = M("weights")->where('id=' . $weights_id)->find();
        $this->assign('model_id', $model['id']);
        $this->assign('weights_id', $weights['id']);
        $this->assign('weights_name', $weights['name']);
        $this->display();
//        echo json_encode($model);
    }

    public function load_results_data($weights_id = 0, $movie_id = 0, $class_id = 0)
    {
        $where['weights_id'] = $weights_id;
        $where['movie_id'] = $movie_id;
        $where['class_id'] = $class_id;
        $where['confidence'] = array('gt', 0.2);
        $ret = M("iou")->where($where)->select();
        for ($i = 0; $i < count($ret); $i++) {
            $ret[$i]['iou'] = $this->get_iou($ret[$i]);
        }
        echo json_encode($ret);
    }


    public function add_testing_data_click($model_id = 0, $weights_id = 0)
    {
        M("verify_list")->where('model_id=' . $model_id . ' and weights_id=' . $weights_id)->delete();

        $sql = "SELECT annotation.class_id, annotation.movie_id, annotation.frame_id, movie.table_name, annotation.filename FROM testing_annotation LEFT JOIN annotation ON annotation.id = testing_annotation.annotation_id LEFT JOIN movie ON movie.id = annotation.movie_id ";
        $sql = str_replace('$model_id', $model_id, $sql);
        $sql = str_replace('$weights_id', $weights_id, $sql);

        $Model = new \Think\Model();
        $ret = $Model->query($sql);

        for ($i = 0; $i < count($ret); $i++) {
            $input['model_id'] = $model_id;
            $input['weights_id'] = $weights_id;
            $input['class_id'] = $ret[$i]['class_id'];
            $input['movie_id'] = $ret[$i]['movie_id'];
            $input['frame_id'] = $ret[$i]['frame_id'];
            $input['folder_name'] = $ret[$i]['table_name'];
            $input['filename'] = $ret[$i]['filename'];
            M("verify_list")->field('model_id,weights_id,class_id,movie_id,frame_id,folder_name,filename')->add($input);
        }
        echo count($ret);
    }

    public function add_sift_annotation($model_id, $movie_id, $class_id, $frames)
    {
        $Movie = M("movie")->where('id=' . $movie_id)->find();
        $input['class_id'] = $class_id;
        $input['movie_id'] = $movie_id;
        $input['filename'] = $frames['filename'];
        $input['w'] = $Movie['w'];
        $input['h'] = $Movie['h'];
        $input['x1'] = $frames['y1'];
        $input['y1'] = $frames['x1'];
        $input['x2'] = $frames['x2'];
        $input['y2'] = $frames['y2'];
        $input['round'] = 1;

        $annotation = M("annotation");
        $ret = $annotation->field('class_id,movie_id,filename,w,h,x1,y1,x2,y2,round')->add($input);
        echo $ret . ' ';
    }

    public function create_sift_annotation($model_id, $movie_id, $class_id)
    {
        $Movie = M("movie")->where('id=' . $movie_id)->find();
        $count = $this->get_count1($model_id, $movie_id, $class_id, $Movie);
        for ($i = 0; $i < 25; $i++) {
            $id = rand(0, $count);

            $Model = new \Think\Model();
            $sql = "SELECT frames.id,
        frames.filename,
        frames.time,
        frames.keyframe,
        frames.faces,
        gt_frame.gt,
        results.confidence,
        results.x1,
        results.y1,
        results.x2,
        results.y2
      FROM 01_tangled
      LEFT JOIN gt_frame
      ON frames.id = gt_frame.frame_id
      JOIN results
      ON frames.id = results.frame_id
      WHERE
      results.confidence > 0.5 AND
      results.class_id = $class_id AND
      results.movie_id = $movie_id AND
      results.model_id = $model_id LIMIT $id,1";

            $sql = str_replace('$class_id', $class_id, $sql);
            $sql = str_replace('$movie_id', $movie_id, $sql);
            $sql = str_replace('$model_id', $model_id, $sql);
            $sql = str_replace('$id', $id, $sql);

            $frames = $Model->query($sql);
            $annotation = M("annotation")->where('filename=\'' . $frames[0]['filename'] . '\'')->find();
            // echo json_encode($frames[0]['filename']);
            if ($annotation == null) {
                $this->add_sift_annotation($model_id, $movie_id, $class_id, $frames[0]);
            }
        }
//       echo json_encode($sql);
    }

    public function get_count1($model_id, $movie_id, $class_id, $Movie, $confidence = 0.2)
    {
        $Model = new \Think\Model();
        $sql = "SELECT
        count(*)
      FROM frames
      LEFT JOIN gt_frame
      ON frames.id = gt_frame.frame_id
      JOIN results
      ON frames.id = results.frame_id
      WHERE
        results.confidence > 0.5 AND
      results.class_id = $class_id AND
      results.movie_id = $movie_id AND
      results.model_id = $model_id AND
      results.confidence > $confidence";

        $sql = str_replace('$class_id', $class_id, $sql);
        $sql = str_replace('$movie_id', $movie_id, $sql);
        $sql = str_replace('$model_id', $model_id, $sql);
        $sql = str_replace('$confidence', $confidence, $sql);

        $ret = $Model->query($sql);
        return $ret[0]['count(*)'];
    }

    public function get_count($model_id, $movie_id, $class_id, $Movie, $confidence = 0.2)
    {
        $Model = new \Think\Model();
        $sql = "SELECT
        count(*)
      FROM frames
      LEFT JOIN gt_frame
      ON frames.id = gt_frame.frame_id
      JOIN results
      ON frames.id = results.frame_id
      WHERE
      results.class_id = $class_id AND
      results.movie_id = $movie_id AND
      results.model_id = $model_id AND
      results.confidence > $confidence";

        $sql = str_replace('$class_id', $class_id, $sql);
        $sql = str_replace('$movie_id', $movie_id, $sql);
        $sql = str_replace('$model_id', $model_id, $sql);
        $sql = str_replace('$confidence', $confidence, $sql);

        $ret = $Model->query($sql);
        return $ret[0]['count(*)'];
    }

    public function get_results($model_id, $movie_id, $class_id, $confidence = 0.2, $minute = 0)
    {
        $Movie = M("movie")->where('id=' . $movie_id)->find();
        $count = $this->get_count($model_id, $movie_id, $class_id, $Movie);

        $Model = new \Think\Model();
        $sql = "SELECT
        frames.id,
        frames.filename,
        frames.time,
        frames.keyframe,
        frames.faces,
        gt_frame.gt,
        results.confidence,
        results.x1,
        results.y1,
        results.x2,
        results.y2
      FROM frames
      LEFT JOIN gt_frame
      ON frames.id = gt_frame.frame_id
      JOIN results
      ON frames.id = results.frame_id
      WHERE 
      results.class_id = $class_id AND
      results.movie_id = $movie_id AND
      results.model_id = $model_id AND
      results.confidence > $confidence
      LIMIT 1440*$minute,1440";

        $sql = str_replace('$minute', $minute, $sql);
        $sql = str_replace('$class_id', $class_id, $sql);
        $sql = str_replace('$movie_id', $movie_id, $sql);
        $sql = str_replace('$model_id', $model_id, $sql);
        $sql = str_replace('$confidence', $confidence, $sql);

        $frames = $Model->query($sql);

        $ret['w'] = $Movie['w'];
        $ret['h'] = $Movie['h'];
        $ret['count'] = $count;
        $ret['pages'] = $this->get_pages($model_id, $movie_id, $class_id, $confidence, $minute, $count);
        $ret['frames'] = $frames;
        $ret['minute'] = $minute;
        $ret['movie_id'] = $movie_id;
        $ret['image_path'] = C('IMAGE_PATH') . $Movie['table_name'] . '/all_frame/';

        echo json_encode($ret);
    }

    public function results_frames($movie_id = 0, $weights_id = 0, $confidence = 0.2, $minute = 0)
    {
        $movies = M("movie")->where('id=' . $movie_id)->find();
        $weights = M("weights")->where('id=' . $weights_id)->find();
        if (count($movies) > 0) {
            $this->assign('weights_name', $weights['name']);
            $this->assign('movie_name', $movies['name']);
            $this->assign('movie_id', $movie_id);
            $this->assign('weights_id', $weights_id);
            $this->assign('minute', $minute);
            $this->assign('confidence', $confidence);
        }
        $this->display();
    }

    public function get_results_pages($weights_id = 0, $movie_id = 0, $confidence = 0.2, $minute = 0, $count)
    {
        $pages = array();
        if ($minute > 0) {
            $data = array();
            $data['name'] = 'Prev';
            $data['class'] = 'page-item';
            $data['link'] = "/ai/models/results_frames/movie_id/" . $movie_id . '/weights_id/' . $weights_id . '/confidence/' . $confidence . '/minute/' . ($minute - 1);
            array_push($pages, $data);
        }

        $start = ($minute - 9) > 0 ? ($minute - 9) : 0;

        for ($i = $start; $i < ($count / (24 * 60)) + 1; $i++) {
            $data = array();
            $data['name'] = $i;
            if ($i == $minute) {
                $data['class'] = 'page-item active';
            } else {
                $data['class'] = 'page-item';
            }

            $data['link'] = "/ai/models/results_frames/movie_id/" . $movie_id . '/weights_id/' . $weights_id . '/confidence/' . $confidence . '/minute/' . $i;
            array_push($pages, $data);
            if (count($pages) > 19) {
                break;
            }
        }

        if ($minute < ($count / (24 * 60))) {
            $data = array();
            $data['name'] = 'Next';
            $data['class'] = 'page-item';
            $data['link'] = "/ai/models/results_frames/movie_id/" . $movie_id . '/weights_id/' . $weights_id . '/confidence/' . $confidence . '/minute/' . ($minute + 1);
            array_push($pages, $data);
        }
        return $pages;
    }


    public function get_results_frames($weights_id = 0, $movie_id = 0, $confidence = 0.2, $minute = 0)
    {
        $movies = M("movie")->where('id=' . $movie_id)->find();

        $results_model = new \Think\Model();
        $sql = 'SELECT frame_id, results.filename, class_id, class_index, confidence, w, h, x1, y1, x2, y2 FROM results
LEFT JOIN movie ON movie.id = results.movie_id
WHERE weights_id = ' . $weights_id . ' AND movie_id = ' . $movie_id . ' AND confidence > ' . $confidence .
            ' ORDER BY frame_id LIMIT ' . $minute * 24 * 60 . ',' . 24 * 60;
        $results_frames = $results_model->query($sql);

        $count_model = new \Think\Model();
        $count_sql = 'SELECT count(*) FROM results
WHERE weights_id = ' . $weights_id . ' AND movie_id = ' . $movie_id . ' AND confidence > ' . $confidence;
        $count_result = $count_model->query($count_sql);

        $count = $count_result[0]['count(*)'];

        $ret['count'] = $count;
        $ret['pages'] = $this->get_results_pages($weights_id, $movie_id, $confidence, $minute, $count);
        $ret['frames'] = $results_frames;
        $ret['minute'] = $minute;
        $ret['movie_id'] = $movie_id;
        $ret['image_path'] = C('IMAGE_PATH') . $movies['table_name'] . '/all_frame/';
        echo json_encode($ret);
    }

    public function get_detail($model_id = 0, $class_id = 0)
    {
        $results_model = new \Think\Model();
        $sql = 'SELECT training_data.id, training_data.class_id, training_data.movie_id, training_data.model_id, training_data.filename, training_data.w,
training_data.h, training_data.x1, training_data.y1, training_data.x2, training_data.y2, movie.table_name
FROM training_data
JOIN movie ON training_data.movie_id = movie.id
WHERE training_data.model_id = $model_id AND training_data.class_id = $class_id';
        $sql = str_replace('$model_id', $model_id, $sql);
        $sql = str_replace('$class_id', $class_id, $sql);
        $count_result = $results_model->query($sql);
        echo json_encode($count_result);
    }


}
