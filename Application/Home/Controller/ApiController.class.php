<?php
namespace Home\Controller;
use Think\Controller;
class ApiController extends Controller {
  public function getframe($minute = ""){
    $ret['frames'] =
    $frames = M("01_tangled")->limit($minute*24*60, 24*60)->select();
    $ret['frames'] = $frames;
    $ret['minute'] = $minute;
    echo json_encode($ret);
  }

  public function saveframe(){
    $rawpostdata = file_get_contents("php://input");
    $post = json_decode($rawpostdata, true);
    foreach ($post as $value){
      $frame = M("01_tangled");
      $frame->gt = $value['gt'];
      $frame->where('id='.$value['id'])->save();
    }
    echo $rawpostdata;
  }

  public function cacl(){
    $a = exec("python ../GT/cacl/json_cacl.py");
    // $a = shell_exec("whoami");
    //$path = shell_exec("pwd");
    //$a = shell_exec("python ../GT/convert_tensorflow.py");
    echo $a;
  }

  public function upload_json_results(){
    //import('ORG.Net.UploadFile');
    $upload = new \Think\Upload();// 实例化上传类
    $upload->maxSize  = 1024*1024*100 ;// 设置附件上传大小
    $upload->allowExts  = array('tar','gz');// 设置附件上传类型
    $upload->rootPath = './Uploads/';
    $upload->savePath =  '';// 设置附件上传目录
    // $info = $upload->uploadOne($_FILES['photo1']);
    // if(!$info) {// 上传错误提示错误信息
    //   $this->error($upload->getError());
    // }else{// 上传成功 获取上传文件信息
    //   echo $info['savepath'].$info['savename'];
    // }
    $info = $upload->upload();
    if(!$info) {// 上传错误提示错误信息
      $this->error($upload->getError());
    }else{// 上传成功 获取上传文件信息
      $path = exec("pwd");
      $file = './Uploads/' . $info['file']['savepath'] . $info['file']['savename'];
      // $s = exec("tar -xzvf " . $file . ' -C ./AIDATA/01_Tangled/verify_results');
      $s = exec("tar -xzvf " . $file . ' -C ../GT');
      // $this->success(json_encode($info));
      echo $s;
    }
  }
}
