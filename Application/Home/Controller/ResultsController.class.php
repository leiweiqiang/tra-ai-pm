<?php

namespace Home\Controller;

use Think\Controller;

class ResultsController extends Controller
{
    public function index()
    {
        $this->display();
    }

    public function results($model_id, $minute=0)
    {
        $model = M("model")->where('id=' . $model_id)->find();
        $this->assign('model_id', $model['id']);
        $this->assign('model_name', $model['name']);
        $this->assign('minute', $minute);
        $this->display();
        // echo json_encode($model);
    }

    public function get_pages($model_id, $movieid, $minute, $count)
    {
        $pages = array();
        if ($minute > 0) {
            $data = array();
            $data['name'] = 'Prev';
            $data['class'] = 'page-item';
            $data['link'] = '/ai/results/results/model_id/' . $movieid . '/minute/' . ($minute - 1);
            array_push($pages, $data);
        }

        $start = ($minute - 9) > 0 ? ($minute - 9) : 0;

        for ($i = $start; $i < ($count / (24 * 60)) + 1; $i++) {
            $data = array();
            $data['name'] = $i;
            if ($i == $minute) {
                $data['class'] = 'page-item active';
            } else {
                $data['class'] = 'page-item';
            }

            $data['link'] = '/ai/results/results/model_id/' . $model_id . '/minute/' . $i;
            array_push($pages, $data);
            if (count($pages) > 19) {
                break;
            }
        }

        if ($minute < ($count / (24 * 60))) {
            $data = array();
            $data['name'] = 'Next';
            $data['class'] = 'page-item';
            $data['link'] = '/ai/results/results/model_id/' . $model_id . '/minute/' . ($minute + 1);
            array_push($pages, $data);
        }
        return $pages;
    }

    public function get_results($model_id, $movie_id, $minute = 0)
    {
        $Movie = M("movie")->where('id=' . $movie_id)->find();
        $Frames = M('frames')->where('movie_id=' . $movie_id);
        $count = $Frames->count();

        $Model = new \Think\Model();
        $sql = "SELECT
        01_tangled.id,
        01_tangled.filename,
        01_tangled.time,
        01_tangled.keyframe,
        01_tangled.faces,
        gt_frame.gt,
        results.confidence,    
        results.x1,
        results.y1,
        results.x2,
        results.y2
      FROM frames
      LEFT JOIN gt_frame
      ON frames.id = gt_frame.frame_id
      JOIN results
      ON frames.id = results.frame_id
      where frames.movie_id = $movie_id 
      limit 1440*$minute,1440";

        $sql = str_replace('$movie_id', $movie_id, $sql);
        $sql = str_replace('$minute', $minute, $sql);

        $frames = $Model->query($sql);

        $ret['w'] = $Movie['w'];
        $ret['h'] = $Movie['h'];
        $ret['count'] = $count;
        $ret['pages'] = $this->get_pages($model_id, $movie_id, $minute, $count);
        $ret['frames'] = $frames;
        $ret['minute'] = $minute;
        $ret['movie_id'] = $movie_id;
        $ret['image_path'] = C('IMAGE_PATH') . $Movie['table_name'] . '/all_frame/';

        echo json_encode($ret);
    }
}
